package com.example.jharshman.jdharshmanlab7;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, View.OnDragListener {

    // game pieces
    ImageView mXPiece;
    ImageView mOPiece;

    // game board
    ImageView mR1C1;
    ImageView mR1C2;
    ImageView mR1C3;
    ImageView mR2C1;
    ImageView mR2C2;
    ImageView mR2C3;
    ImageView mR3C1;
    ImageView mR3C2;
    ImageView mR3C3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // find the resource in the layout
        mXPiece = (ImageView)findViewById(R.id.x_game_piece);
        mOPiece = (ImageView)findViewById(R.id.o_game_piece);

        mXPiece.setTag(R.drawable.x);
        mOPiece.setTag(R.drawable.o);

        // register with the touch listener
        mXPiece.setOnTouchListener(this);
        mOPiece.setOnTouchListener(this);

        // find the game board
        mR1C1 = (ImageView)findViewById(R.id.row_one_col_one);
        mR1C2 = (ImageView)findViewById(R.id.row_one_col_two);
        mR1C3 = (ImageView)findViewById(R.id.row_one_col_three);
        mR2C1 = (ImageView)findViewById(R.id.row_two_col_one);
        mR2C2 = (ImageView)findViewById(R.id.row_two_col_two);
        mR2C3 = (ImageView)findViewById(R.id.row_two_col_three);
        mR3C1 = (ImageView)findViewById(R.id.row_three_col_one);
        mR3C2 = (ImageView)findViewById(R.id.row_three_col_two);
        mR3C3 = (ImageView)findViewById(R.id.row_three_col_three);

        mR1C1.setOnDragListener(this);
        mR1C2.setOnDragListener(this);
        mR1C3.setOnDragListener(this);
        mR2C1.setOnDragListener(this);
        mR2C2.setOnDragListener(this);
        mR2C3.setOnDragListener(this);
        mR3C1.setOnDragListener(this);
        mR3C2.setOnDragListener(this);
        mR3C3.setOnDragListener(this);


    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        if(event.getAction() == DragEvent.ACTION_DROP) {
            View dragged = (View)event.getLocalState();
            int resId = (int)dragged.getTag();
            ImageView imageView = (ImageView)v;
            imageView.setImageResource(resId);
            imageView.setTag(resId);
            Log.d("DEBUG: setting tag: ", "tag " + resId);
        }
        return true;
    }

    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            // create a new DragShadowBuilder object
            // pass the view param
            View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(v);
            // call startDrag method
            v.startDrag(null, dragShadowBuilder, v, 0);
            // return true for ACTION_DOWN event
            return true;
        }
        // return false for other actions
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
